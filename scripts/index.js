// 1. Опишіть своїми словами як працює метод forEach.

//      Метод forEach перебирає елементи массиву та дозволяє запускати функцію для кожного елемента масиву. 

// 2. Як очистити масив?

//      Поки довжина масиву true в циклі видалаємо елементи 
//      while (arr.length) { arr.pop(); }

// 3. Як можна перевірити, що та чи інша змінна є масивом?

//      За допомогою методу isArray(); Array.isArray(arr); => true || false 

let number1Hint = document.querySelector('.number1-hint');
let number2Hint = document.querySelector('.number2-hint');

const errorHandler = (text, place) => {
    if (place === 'array') {
        number1Hint.innerText = text;
    } else if (place === 'type') {
        number2Hint.innerText = text;
    }
};

const action1 = document.querySelector('.input-num1');
const action2 = document.querySelector('.input-num2');

const validate = (array, type) => {
    let switcher = true;

    if (!array) {
        errorHandler('Ви не ввели array!', 'array');

        switcher = false;
    }

    if (!type) {
        errorHandler('Ви не ввели тип!', 'type');

        switcher = false;
    }

    return switcher;
};

const filterBy = () => {
    const firstArray = document.querySelector('.input-num1');
    const type = document.querySelector('.input-num2');
    const output = document.querySelector('.output');

    number1Hint.innerText = '';
    number2Hint.innerText = '';
    output.innerText = '';

    if (validate(firstArray.value, type.value)) {
        const switcher = type.value.toLowerCase();
        const inputArray = firstArray.value.split(', ');
        const newArray = inputArray.filter(item => {
            if (switcher === 'boolean') return item.toLowerCase() !== 'true' && item.toLowerCase() !== 'false';

            if ((switcher === 'number' || switcher === 'bigint') && !Number(item)) return item;

            if (switcher === 'string')
                return +item === Number(item) || item.toLowerCase() === 'true' || item.toLowerCase() === 'false' || item.toLowerCase() === 'null' || item.toLowerCase() === 'undefined';

            if (switcher === 'null') return item.toLowerCase() !== 'null';

            if (switcher === 'undefined') return item.toLowerCase() !== 'undefined';
        });

        output.innerText = newArray;
        console.log(newArray);
    }
};

const onSubmit = () => {
    const submitBtn = document.querySelector('.submit');

    submitBtn.addEventListener('click', filterBy);
};

onSubmit();
